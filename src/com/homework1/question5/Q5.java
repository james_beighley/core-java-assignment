package com.homework1.question5;


public class Q5 {
	//take the substring without using a substring or stringbuilder
	//by creating a char array of the size you want the substring to be
	//then insert each char from the original string that you wish to keep.
	//then return that char array as a string.
	
	public static String subString(String s, int lastIndex) {
		char[] chararray = new char[lastIndex];
		for(int i = 0; i< lastIndex; i++) {
			chararray[i] = s.charAt(i);
			System.out.println(chararray[i]);
		}
		return String.valueOf(chararray);
	}
	
	public static void main(String[] args) {
		String s = "abcdefg";
		String mySubstring = subString(s, 2);
		System.out.println(mySubstring);
	}

}
