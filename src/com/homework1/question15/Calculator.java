package com.homework1.question15;

//calculator object with methods for addition, subtraction,
//multiplication and division

public class Calculator implements OperationsInterface{

	public int addition(int a, int b) {
		return a + b;
	}
	public int subtraction(int a, int b) {
		return a-b;
	}
	public int multiplication(int a, int b) {
		return a*b;
	}
	public int division(int a, int b) {
		return a/b;
	}

}
