package com.homework1.question15;

//main method to test out calculator object and it's behavior

public class Q15 {

	public static void main(String[] args) {
		Calculator calculator = new Calculator();
		int a = calculator.multiplication(15, 4);
		int b = calculator.addition(11, 26);
		System.out.println(a);
		System.out.println(b);
	}

}
