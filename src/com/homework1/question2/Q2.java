package com.homework1.question2;

public class Q2 {
	/*
	 * The Fibonacci sequence is created by adding the previous two 
	 * numbers that come before it, where the first two numbers are
	 * 0 and 1
	 */
	public static void main(String[] args) {
		int firstNum = 0;
		int secondNum = 1;
		System.out.print(firstNum + ", " + secondNum);
		for(int i = 0; i< 23; i++){
			int currentNum = firstNum + secondNum;
			System.out.print(", " + currentNum);
			firstNum = secondNum;
			secondNum = currentNum;
		}

	}

}
