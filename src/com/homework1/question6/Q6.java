package com.homework1.question6;
import java.util.Scanner;

public class Q6 {
	/*
	 * To check if a number is even without using the modulus operator,
	 * you can divide the number by two and then multiply it back again,
	 * then check for equivalence with the original number.
	 * This works because division in java will round down if it does 
	 * not divide evenly, the attempt to return to the original by 
	 * multiplying it back will show whether it was originally odd or 
	 * even if it goes back to the original number or not.
	 */
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter a positive whole number");
		int num = myScanner.nextInt();
		myScanner.close();
		int half = num / 2;
		half = half * 2;
		if(num == half) 
			System.out.println("Your number is even");
		else
			System.out.println("Your number is odd");
	}

}