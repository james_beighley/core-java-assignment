package com.homework1.question8;

import java.util.ArrayList;

public class Q8 {
	//method to check whether or not a string is a palindrome
	//by checking the start and end characters and incrementing
	//and decrementing the start and end index until either
	// finding unequal or passing each other, making it
	// a valid palindrome
	public static boolean isPalindrome(String s) {
		int start = 0;
		int end = s.length()-1;
		while(end>start) {
			if(s.charAt(start)!= s.charAt(end))
				return false;
			end--;
			start++;
		}
		return true;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String[] names = {"karan", "madam", "tom", "civic", "radar", "sexes", "jimmy", "kayak", "john", "refer", "billy", "did"};
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> palindromes = new ArrayList<String>();
		for(int i = 0; i< names.length; i++) {
			list.add(names[i]);
			if(isPalindrome(names[i]))
				palindromes.add(names[i]);
		}
		for(int i = 0; i< palindromes.size(); i++) {
			System.out.println(palindromes.get(i));
		}
	}

}
