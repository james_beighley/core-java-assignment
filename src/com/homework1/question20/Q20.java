package com.homework1.question20;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class Q20 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			File file = new File("src/com/homework1/question20/Data.txt");
			Scanner scan = new Scanner(file);
			while(scan.hasNextLine()) {
				String data = scan.nextLine();
				String[] separate = data.split(":");
				System.out.println("name: " + separate[0] + " " + separate[1]);
				System.out.println("age: " + separate[2] + " years");
				System.out.println("state: " + separate[3] + " State");
			}
			scan.close();
		}
		catch(FileNotFoundException e) {
			System.out.println("No file was found");
			e.printStackTrace();
		}

		
	}

}
