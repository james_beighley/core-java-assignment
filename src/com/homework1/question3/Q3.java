package com.homework1.question3;

public class Q3 {
	//to reverse without using reverse or a temporary variable,
	//concat the end of the string with each character in reverse
	//order, then substring from the halfway point of the new string.
	//This halfway point is guaranteed to be half of the new length
	//string because we effectively just doubled the original length.
	public static String reverse(String s) {
		for(int i = s.length()-1; i>=0; i--) {
			s = s.concat(String.valueOf(s.charAt(i)));
		}
		return s.substring(s.length()/2, s.length());
	}

	public static void main(String[] args) {
		String s = "please reverse me";
		s = reverse(s);
		System.out.println(s);

	}

}
