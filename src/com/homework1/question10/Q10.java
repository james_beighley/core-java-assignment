package com.homework1.question10;

public class Q10 {
	//ternary operater: conditional, then question mark, if true first condition, else
	//condition after the colon mark.
	
	public static int min(int a, int b) {
		int minimum = (a<b)? a : b;
		return minimum;
	}
	
	public static void main(String[] args) {
		int a = 25;
		int b = 35;
		int c = min(a,b);
		System.out.println(c);
	}

}
