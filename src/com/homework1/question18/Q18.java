package com.homework1.question18;
import java.util.Scanner;
public class Q18 {

	public static void main(String[] args) {
		MySubClass myclass = new MySubClass();
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter a String, I will check for uppercase characters, convert them to lowercase, and add 10 to the string's integer representation");
		String s = scan.next();
		scan.close();
		if(myclass.anyUpper(s)) {
			System.out.println("Uppercase detected, here is your string with them converted to lowercase:");
			System.out.println(myclass.convertToLowerCase(s));
		}
		else {
			System.out.println("No Uppercase detected");
		}
		System.out.println("here is your string to an int with 10");
		System.out.println(myclass.convertToIntAndAddTen(s));
	}

}
