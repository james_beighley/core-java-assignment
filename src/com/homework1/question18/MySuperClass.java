package com.homework1.question18;

public abstract class MySuperClass {
	public abstract boolean anyUpper(String s);
	public abstract String convertToLowerCase(String s);
	public abstract int convertToIntAndAddTen(String s);
}