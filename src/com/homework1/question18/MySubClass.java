package com.homework1.question18;

public class MySubClass extends MySuperClass{

	@Override
	public boolean anyUpper(String s) {
		for(int i = 0; i< s.length(); i++) {
			if(s.charAt(i)>64 && s.charAt(i)<91)
				return true;
		}
		return false;
	}

	@Override
	public String convertToLowerCase(String s) {
		return s.toLowerCase();
	}

	@Override
	public int convertToIntAndAddTen(String s) {
		int total = 0;
		for(int i = 0; i< s.length(); i++) {
			total = total + (int)s.charAt(i);
		}
		return total + 10;
	}

}
