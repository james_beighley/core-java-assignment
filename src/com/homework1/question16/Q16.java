package com.homework1.question16;

//use the args array to pass in data from the command line
//and the string length() method to get length of string
//passed in

public class Q16 {
	
	public static void main(String[] args) {
		System.out.println("number of characters in " + args[0] + " is " + args[0].length());
	}

}
