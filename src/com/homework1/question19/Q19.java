package com.homework1.question19;

import java.util.ArrayList;

public class Q19 {
	
	public static boolean isPrime(int num) {
		for(int i = 2; i< num; i++) {
			if(num%i==0)
				return false;
		}
		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> list = new ArrayList<Integer>();
		int oddtotal = 0;
		int eventotal = 0;
		for(int i = 1; i<=10; i++) {
			list.add(i);
			if(i%2==0) {
				eventotal += i;
			}
			else {
				oddtotal += i;
			}
		}
		System.out.println(list);
		System.out.println(eventotal);
		System.out.println(oddtotal);
		for(int i = 0; i<list.size(); i++) {
			if(isPrime(list.get(i))) {
				list.remove(i);
				i--;
			}
		}
		System.out.println(list);
	}

}
