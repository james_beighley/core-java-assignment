package com.homework1.question1;

public class Q1{
	
	/*
	 * bubble sort method, which takes an array of integers as input
	 * and sorts them in ascending order, O(n^2) efficiency
	 */
	public static void bubbleSort(int[] arr){
		for(int i = 0; i< arr.length; i++) {
			for(int j = i; j< arr.length; j++) {
				if(arr[i]> arr[j]){
					
					// temporary value used to swap i and j
					
					int temp = arr[i]; 
					arr[i] = arr[j];
					arr[j] = temp;
				}
			}
		}
	}
	/*
	 * main method instantiates an array of integers and calls the
	 * bubble sort method on them, once the array is sorted the 
	 * array will be printed to the console
	 */
	public static void main(String[] args){
		int[] arr = {1,0,5,6,3,2,3,7,9,8,4};
		bubbleSort(arr);
		for(int i = 0; i< arr.length; i++){
			System.out.println(arr[i]);
		}
	}
}
