package com.homework1.question14;

import java.util.Scanner;
import java.util.Date;
import java.lang.Math;
import java.text.SimpleDateFormat;

//use a scanner to test out the switch case.
//use the date object and formatter to get the
//current date in the format you want.

public class Q14 {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("type '1' for a square root");
		System.out.println("type '2' for today's date");
		System.out.println("type '3' for a string array");
		int i = scan.nextInt();
		scan.close();
		switch(i) {
			case 1:
				int squared = 81;
				double squareroot = Math.sqrt(squared);
				System.out.println("The square root of " + squared + " is " + (int)squareroot);
				break;
			case 2:
				Date date = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				System.out.println(formatter.format(date));
				break;
			case 3:
				String s = "I am learning Core Java";
				String[] stringarray = s.split(" ");
				for(int a = 0; a< stringarray.length; a++) {
					System.out.println(stringarray[a]);
				}
				
				break;
			default:
				System.out.println("wrong input");
		}
		
	}

}
