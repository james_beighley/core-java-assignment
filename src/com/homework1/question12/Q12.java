package com.homework1.question12;

public class Q12 {
	
	//fill up array and print out even numbers
	//array filled with i+1 so i can still access
	//the needed index, but assign the value
	//we want. index % 2 == 0 ensures that
	//there is no remainder when dividing by 2,
	//hence even values are printed
	
	public static void main(String[] args) {
		int[] myArray = new int[100];
		for(int i = 0; i< myArray.length; i++) {
			myArray[i] = i+1;
		}
		for(int index : myArray) {
			if(index % 2 == 0) {
				System.out.println(index);
			}
		}
	}

}
