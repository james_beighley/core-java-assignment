package com.homework1.question9;

import java.util.ArrayList;

public class Q9 {
	
	//method to check if a number is prime
	//make sure it doesn't evenly divide with
	//numbers up to half of its value, over
	//half couldn't be a factor
	
	public static boolean isPrime(int a) {
		if(a<=1)
			return false;
		for(int i = 2; i< a/2; i++) {
			if(a % i == 0)
				return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Integer> list = new ArrayList<Integer>();
		for(int i = 1; i<= 100; i++) {
			if(isPrime(i)) {
				System.out.print(i + " ");
			}
			list.add(i);
		}
	}

}
