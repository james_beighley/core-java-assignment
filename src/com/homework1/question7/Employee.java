package com.homework1.question7;

public class Employee {
	String name;
	String department;
	int age;
	public Employee(String name, String department, int age) {
		this.name = name;
		this.department = department;
		this.age = age;
	}
}