package com.homework1.question7;

import java.util.Comparator;

public class StringComparer<String> implements Comparator<String> {
	public StringComparer(){
	}

	@Override
	public int compare(String o1, String o2) {
		if(o1.equals(o2))
			return 0;
		else {
			return 1;
		}
	}

}
