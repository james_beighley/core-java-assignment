package com.homework1.question7;

import java.util.Comparator;

public class IntegerComparer<Integer> implements Comparator<Integer> {
	public IntegerComparer(){
	}

	@Override
	public int compare(Integer o1, Integer o2) {
		if((int)o1>(int)o2) 
			return 1;
		else if(o1.equals(o2)) 
			return 0;
		else
			return -1;
	}

}
