package com.homework1.question7;

public class Q7 {
	
	//I'm not sure if this is what the question was asking for
	//but I can't figure out how to make the comparison of greater
	//or less for a string so i had to settle for equivalence
	//and non-equivalence
	
	
	public static void printemployee(Employee m) {
		System.out.println(m.name +", " + m.age + ", " + m.department);
	}
	
	public static void main(String[] args) {
		Employee first = new Employee("james","health",23);
		Employee second = new Employee("david","hr",33);
		StringComparer<String> stringcomparer = new StringComparer<String>();
		IntegerComparer<Integer> integercomparer = new IntegerComparer<Integer>();
		System.out.println("when compared by Age:");
		if(integercomparer.compare(first.age, second.age) == 1) {
			printemployee(first);
			printemployee(second);
		}
		else if(integercomparer.compare(first.age, second.age) == 0) {
			System.out.println("they are the same age");
		}
		else {
			printemployee(second);
			printemployee(first);
		}
		System.out.println("when compared by Department:");
		if(stringcomparer.compare(first.department, second.department) == 1) {
			System.out.println("They do not work in the same department");
		}
		else {
			System.out.println("they work in the same department");
		}
		System.out.println("when compared by Name:");
		if(stringcomparer.compare(first.name, second.name) == 1) {
			System.out.println("They do not have the same name");
		}
		else{
			System.out.println("they have the same name");
		}
	}

}
