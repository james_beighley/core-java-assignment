package com.homework1.question13;

import java.util.ArrayList;

public class Q13 {

	//to print out the pyramid of 0's and 1's, make an array list of array lists
	//and fill it with the pattern you want.
	//then use a nested for loop to print it all out.
	
	public static void main(String[] args) {
		ArrayList<ArrayList<Integer>> list = new ArrayList<ArrayList<Integer>>(4);
		list.add(new ArrayList<Integer>());
		list.get(0).add(0);
		list.add(new ArrayList<Integer>());
		list.get(1).add(1);
		list.get(1).add(0);
		list.add(new ArrayList<Integer>());
		list.get(2).add(1);
		list.get(2).add(0);
		list.get(2).add(1);
		list.add(new ArrayList<Integer>());
		list.get(3).add(0);
		list.get(3).add(1);
		list.get(3).add(0);
		list.get(3).add(1);
		for(int i = 0; i< 4; i++) {
			for(int j =0; j<list.get(i).size(); j++)
				System.out.print(list.get(i).get(j)+ " ");
			System.out.println();
		}
	}

}
