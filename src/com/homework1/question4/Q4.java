package com.homework1.question4;
import java.util.Scanner;

public class Q4 {
	/*
	 * a factorial is a mathematical operation such that the
	 * given number is multiplied by all positive non-zero
	 * integers below it. If the number is 0, the factorial
	 * will simply return 1.
	 */
	public static int factorial(int n) {
		int factorial = 1;
		if(n != 0) {
			while(n>1) {
				factorial *= n;
				n--;
			}
		}
		return factorial;
	}
	/*
	 * Since the number was not specified on the problem,
	 * I used a scanner to take a number input by 
	 * the user and return the factorial of that number
	 */
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter a positive whole number");
		int num = myScanner.nextInt();
		myScanner.close();
		int factorialedNum = factorial(num);
		System.out.println("The Factorial of your number is: " + factorialedNum);
	}

}